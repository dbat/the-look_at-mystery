Solved
==
GrimBeanXXL on Discord#Godot#Beginners helped me fix this. 

The solution was to add the current translation of the arrow to the new velocity in the look_at function.

1. velocity = velocity.bounce(collision.normal)
2. look_at(translation + velocity, Vector3.UP)

Breakdown
--
I did not grok this at first, so I plotted out all the vectors to have a look. I printed them out and put in a breakpoint after a collision.

The initial vector:
velocity:(2.80886, 0, -2.84786) # so, going up and right
The bounce:
bounce_velocity:(2.807593, -0.000408, 2.849108) # down and right

The actual location on the screen (node.translation):
translation:(5.577096, -0.000003, -14.597822)
(Is really the transform.origin)
origin:(5.577096, -0.000003, -14.597822)

The sum of the vectors:
translation + velocity:(8.384689, -0.000411, -11.748713)

The sum gives a point on the screen where the actual object will be after it starts to move at velocity from it's current translation position.

So, the mistake I was making was to think of the velocity as an actual place on the screen - it's not! It's only relative to (0,0,0).

In order to get the actual pixel that the arrow should look at, I have to add the position and the relative velocity.

I hope that helps someone else. Thanks GrimBeanXXL!

