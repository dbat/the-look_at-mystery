extends KinematicBody

var speed : float = 4
var velocity : Vector3 = Vector3.ZERO

func _ready():
	velocity = -global_transform.basis.z * speed
	
func _physics_process(delta):
	velocity.y = 0 #to keep it on a level
	var collision = move_and_collide(velocity * speed * delta)
	if collision:
		var bounce_velocity := velocity.bounce(collision.normal)
		##
		## Why does the look_at not actually face the boid towards the direction?
		##
		print("velocity:", velocity)
		velocity = bounce_velocity
		print("bounce_velocity:",bounce_velocity)

		#look_at(velocity.normalized(), Vector3.UP)
		
		# GrimBeanXXL : so our current position + velocity is where we want to point
		# GrimBean got it working with this translation + velocity line. Amazing.
		print("translation:", translation)
		print("origin:",transform.origin)
		print("translation + velocity:", translation + velocity)
		look_at(translation + velocity, Vector3.UP)
		##
		## What I really want is it to gradually rotate to face the
		## forward direction while it's travelling.
